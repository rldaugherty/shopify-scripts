# FREE LOYALTY SHIPPING ********************************

  ELIGIBLE_SERVICES = ['Ground','Standard']
  ELIGIBLE_SERVICES2 = ['Ground','Standard','2nd Day']

  VIP_CUSTOMER_TAG0 = 'accepts_loyalty'
  VIP_CUSTOMER_TAG1 = 'silver'
  VIP_CUSTOMER_TAG2 = 'gold'

  # OVER $50+
  min_discount_order_amount = Money.new(cents:100) * 1
  total = Input.cart.subtotal_price

  new_total = total
  discount_code = Input.cart&.discount_code

  if discount_code
    if discount_code&.is_a?(CartDiscount::FixedAmount)
      discount_amount = discount_code&.amount
      new_total = total - discount_amount
    elsif discount_code&.is_a?(CartDiscount::Percentage)
      discount_percentage = 1.to_d - (discount_code&.percentage / 100)
      new_total = total * discount_percentage
    end
  end

  discount = if new_total > min_discount_order_amount
                1
              else
                0
              end
  message = "Free Shipping For Good Life Rewards Members!"

  if new_total < min_discount_order_amount
  end

  if !Input.cart.customer.nil? and Input.cart.customer.tags.include?(VIP_CUSTOMER_TAG0)
    Input.shipping_rates.each do |shipping_rate|
      if ELIGIBLE_SERVICES.include?(shipping_rate.name)
        shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
      end
    end
  end

  if !Input.cart.customer.nil? and Input.cart.customer.tags.include?(VIP_CUSTOMER_TAG1)
    Input.shipping_rates.each do |shipping_rate|
      if ELIGIBLE_SERVICES.include?(shipping_rate.name)
        shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
      end
    end
  end

  if !Input.cart.customer.nil? and Input.cart.customer.tags.include?(VIP_CUSTOMER_TAG2)
    Input.shipping_rates.each do |shipping_rate|
      if ELIGIBLE_SERVICES2.include?(shipping_rate.name)
        shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
      end
    end
  end

Output.shipping_rates = Input.shipping_rates