# PESHIP20: Free Shipping ======================================
      
      #create your coupon with % off as normal through shopify,
      # then add the same code to this script
      # no end date

      MESSAGE = "Free Shipping"

      if Input.cart.discount_code && Input.cart.discount_code.code == "CU40SALE"
          Input.shipping_rates.each do |shipping_rate|
          next unless shipping_rate.source == "shopify"
          next unless shipping_rate.name == "Standard"
          shipping_rate.apply_discount(shipping_rate.price, message: MESSAGE)
          end
      end
      
      Output.shipping_rates = Input.shipping_rates