################################
#   This block discounts the ITEMS by a percentage off their original price
################################
@percent = Decimal.new(25) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
        next unless product.tags.include?('Collection:Mens') # Defines the targeted items by tags
        line_discount = line_item.line_price * @percent
        message = "25% Off" #discount message shown to customer
        line_item.change_line_price(line_item.line_price - line_discount, message: message) 
end
Output.cart = Input.cart


################################
#   This block discounts the ORDER by a specific amount original price
################################
discount =  Money.new(cents:0) # initial discount. This is required so the script doesn't error out if the threshold isn't met

min_discount_order_amount = Money.new(cents:100) * 100 # Multiplies the number by 100 cents, to fire the discount flag

total = Input.cart.subtotal_price_was
discount = Money.new(cents: 100) * 25 if total > min_discount_order_amount  #If the total is greater than the min_discount_order_amount, update the discount value 
  message = "$25 Off $100" # Discount message shown to customer
  high = Input.cart.line_items[0] # This creates an array list of items in the cart

  Input.cart.line_items.each do |line_item| # This loops over the items array
  if high.variant.price < line_item.variant.price #This compares each price to the previous. If it's higher, it updates the "high" variable with the index of the item in the array
    high = line_item
  end
end

Input.cart.line_items.each do |line_item| # This loops over the items array (again)
  if high == line_item # If the item index matches the "high" variable value, do the magic.
    line_item.change_line_price(line_item.line_price - discount, message: message)
    # It's important to note that you MUST use line_item.line_price to be discounts, or it would take the remaing value off the discounted price
  end
end

Output.cart = Input.cart