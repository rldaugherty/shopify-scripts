
# 2 for $50 Polos *************************
if Input.cart.discount_code && Input.cart.discount_code.code == "promocode"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "promocode2"
else

Input.cart.line_items.each_with_index do |line_item, index|

    items_quantities = 0

    # Loop over all the items to find eligble ones and total
    eligible_items = Input.cart.line_items.select do |line_item|
        product = line_item.variant.product
        !product.gift_card? && product.tags.include?('limited-time-offers')
    end

    total_items = 0

    eligible_items.each do |line_item|
      total_items += line_item.quantity
    end

    if total_items > 1
      Input.cart.line_items.each do |line_item|
        product = line_item.variant.product
        next unless product.tags.include?('limited-time-offers')
        line_item.change_line_price(Money.new(cents:2500*line_item.quantity), message: "2 for $50")
      end
    end

end
end

Output.cart = Input.cart