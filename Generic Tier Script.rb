#################################################
# Generic Tier Discount - Based on entered values
#################################################
# Product discount to use
#   "BYPASS"      => to bypass discount altogether
#   "NOT_NEEDED"  => to check apply discount without discount code
DISCOUNT_COUPON_CODE = 'NOT_NEEDED'

# Product tag to use (if any)
#   "NOT_NEEDED"  => to apply sitewide
DISCOUNT_TAG = 'promo_included'                

# First Tier
FIRST_TIER_DISCOUNT_TYPE = 'FIXED'              # 'PERCENTAGE' or 'FIXED' discount
FIRST_TIER_FIXED_TYPE_LEVEL = "UNIT OVERRIDE"   # 'ORDER', 'LINE', 'UNIT', 'UNIT OVERRIDE': Level where the fixed discount is applied
FIRST_TIER_THRESHOLD_PRICE = 50                 # i.e. buy at least $50
FIRST_TIER_THRESHOLD_QTY = 0                    # i.e. buy at least 2 quantities
FIRST_TIER_DISCOUNT_AMOUNT = 20                 # i.e. 10 = 10% off (if PERCENTAGE discount type) or fixed $20 off/overwrite the unit price (if FIXED discount type)
FIRST_TIER_DISCOUNT_QUANTITY = 0                # i.e. 2 = 2 for price of $20 to overwrite the unit price (if FIXED discount type)
FIRST_TIER_DISCOUNT_MESSAGE = '3 for $9.99'     # Discount message to show

# Second Tier
APPLY_SECOND_TIER_DISCOUNT = 'NO'               # Are we applying a second-tier discount? 'YES' or 'NO'
SECOND_TIER_DISCOUNT_TYPE = 'FIXED'             # 'PERCENTAGE' or 'FIXED' discount
SECOND_TIER_FIXED_TYPE_LEVEL = "ORDER"          # 'ORDER', 'LINE', 'UNIT', 'UNIT OVERRIDE': Level where the fixed discount is applied
SECOND_TIER_THRESHOLD_PRICE = 100               # i.e. buy at least $100
SECOND_TIER_THRESHOLD_QTY = 0                   # i.e. buy at least 5 quantities
SECOND_TIER_DISCOUNT_AMOUNT = 30                # i.e. 30 = 30% off (if PERCENTAGE discount type) or fixed $30 off/overwrite the unit price (if FIXED discount type)
SECOND_TIER_DISCOUNT_QUANTITY = 0               # i.e. 5 = 5 for price of $30 to overwrite the unit price (if FIXED discount type)
SECOND_TIER_DISCOUNT_MESSAGE = '$20 off $100+'  # Discount message to show

# Third Tier
APPLY_THIRD_TIER_DISCOUNT = 'NO'                # Are we applying a third-tier discount? 'YES' or 'NO'
THIRD_TIER_DISCOUNT_TYPE = 'PERCENTAGE'         # 'PERCENTAGE' or 'FIXED' discount
THIRD_TIER_FIXED_TYPE_LEVEL = "ORDER"           # 'ORDER', 'LINE', 'UNIT', 'UNIT OVERRIDE': Level where the fixed discount is applied
THIRD_TIER_THRESHOLD_PRICE = 200                # i.e. buy at least $200
THIRD_TIER_THRESHOLD_QTY = 0                    # i.e. buy at least 10 quantities
THIRD_TIER_DISCOUNT_AMOUNT = 40                 # i.e. 40 = 40% off (if PERCENTAGE discount type) or fixed $40 off/overwrite the unit price (if FIXED discount type)
THIRD_TIER_DISCOUNT_QUANTITY = 0                # i.e. 10 = 10 for price of $40 to overwrite the unit price (if FIXED discount type)
THIRD_TIER_DISCOUNT_MESSAGE = '40% off $200+'   # Discount message to show

# Check to see if we have a coupon
if DISCOUNT_COUPON_CODE != 'BYPASS' and (DISCOUNT_COUPON_CODE == 'NOT_NEEDED' or (Input.cart.discount_code and Input.cart.discount_code.code == DISCOUNT_COUPON_CODE))

  # If the product is tagged, check for the tag - Comment if not needed
  eligible_items = Input.cart.line_items.select do |line_item|
    product = line_item.variant.product
    !product.gift_card? and ((DISCOUNT_TAG != 'NOT_NEEDED' and product.tags.include?(DISCOUNT_TAG)) or (DISCOUNT_TAG == 'NOT_NEEDED'))
   
  end
 
  # Keep a running counter of the total if using product tags (if there is a discount tag to check) or all products
  total_eligible_price = Money.new(cents: 0)
  total_eligible_qty = 0
  eligible_items.each do |line_item|
    total_eligible_price = total_eligible_price + line_item.line_price
    total_eligible_qty = total_eligible_qty + line_item.quantity
  end
 
  # Check each tier to calculate the discount
  if APPLY_THIRD_TIER_DISCOUNT == 'YES' and APPLY_SECOND_TIER_DISCOUNT == 'YES' and (THIRD_TIER_THRESHOLD_PRICE == 0 or (THIRD_TIER_THRESHOLD_PRICE > 0 and total_eligible_price >= Money.new(cents:100) * THIRD_TIER_THRESHOLD_PRICE)) and (THIRD_TIER_THRESHOLD_QTY == 0 or (THIRD_TIER_THRESHOLD_QTY > 0 and total_eligible_qty >= THIRD_TIER_THRESHOLD_QTY)) and (THIRD_TIER_DISCOUNT_QUANTITY == 0 or (THIRD_TIER_DISCOUNT_QUANTITY > 0 and total_eligible_qty >= THIRD_TIER_DISCOUNT_QUANTITY))
      
      # Make sure that the items are sorted by quantity in case we want to split in the future
      eligible_items = eligible_items.sort_by { | line_item | line_item.quantity }

      # Apply the third tier discount (if it qualifies)
      eligible_items.each do |line_item|
        if (DISCOUNT_TAG != 'NOT_NEEDED' and line_item.variant.product.tags.include?(DISCOUNT_TAG)) or DISCOUNT_TAG == 'NOT_NEEDED'
          # Tag prerequisite satisfied (if any) 
          if (THIRD_TIER_DISCOUNT_TYPE == "PERCENTAGE")
            # Applying a percentage discount
            line_item.change_line_price((100 - THIRD_TIER_DISCOUNT_AMOUNT) * 0.01 * line_item.line_price, message: THIRD_TIER_DISCOUNT_MESSAGE)
          else
            # Applying a fixed discount
            if (FIRST_TIER_DISCOUNT_QUANTITY > 0)
              # Applying an 'x for $y' type of discount
              line_price_amount = line_item.change_line_price(THIRD_TIER_DISCOUNT_AMOUNT * line_item.quantity * Money.new(cents:100), message: THIRD_TIER_DISCOUNT_MESSAGE)
            elsif (THIRD_TIER_FIXED_TYPE_LEVEL == 'UNIT')
              # Applying the discount to the units price
              line_price_amount = line_item.line_price - (Money.new(cents:100) * THIRD_TIER_DISCOUNT_AMOUNT * line_item.quantity)
              line_item.change_line_price(line_price_amount, message: THIRD_TIER_DISCOUNT_MESSAGE)
            elsif (THIRD_TIER_FIXED_TYPE_LEVEL == 'LINE')
              # Applying the discount to the line quantity (rare)
              line_price_amount = line_item.line_price - (Money.new(cents:100) * THIRD_TIER_DISCOUNT_AMOUNT)
              line_item.change_line_price(line_price_amount, message: THIRD_TIER_DISCOUNT_MESSAGE)
            elsif (FIRST_TIER_FIXED_TYPE_LEVEL == 'UNIT OVERRIDE')
              # The unit price will be overwritten by a fixed unit price if it's lower than the unit price for the item
              if Money.new(cents:100) * THIRD_TIER_DISCOUNT_AMOUNT * line_item.quantity < line_item.line_price
                line_price_amount = Money.new(cents:100) * THIRD_TIER_DISCOUNT_AMOUNT * line_item.quantity
                line_item.change_line_price(line_price_amount, message: THIRD_TIER_DISCOUNT_MESSAGE)
              end
            else
              # Applying the discount to the order level using a weighted discount at the line level
              line_price_amount = line_item.line_price - (line_item.line_price.cents / total_eligible_price.cents) * THIRD_TIER_DISCOUNT_AMOUNT * Money.new(cents:100)
              line_item.change_line_price(line_price_amount, message: THIRD_TIER_DISCOUNT_MESSAGE)
            end
          end
        end
      end
  elsif APPLY_SECOND_TIER_DISCOUNT == 'YES' and (SECOND_TIER_THRESHOLD_PRICE == 0 or (SECOND_TIER_THRESHOLD_PRICE > 0 and total_eligible_price >= Money.new(cents:100) * SECOND_TIER_THRESHOLD_PRICE)) and (SECOND_TIER_THRESHOLD_QTY == 0 or (SECOND_TIER_THRESHOLD_QTY > 0 and total_eligible_qty >= SECOND_TIER_THRESHOLD_QTY)) and (SECOND_TIER_DISCOUNT_QUANTITY == 0 or (SECOND_TIER_DISCOUNT_QUANTITY > 0 and total_eligible_qty >= SECOND_TIER_DISCOUNT_QUANTITY))
      
      # Make sure that the items are sorted by quantity in case we want to split in the future
      eligible_items = eligible_items.sort_by { | line_item | line_item.quantity }

      # Apply the second tier discount (if it qualifies)
      Input.cart.line_items.each do |line_item|
        if (DISCOUNT_TAG != 'NOT_NEEDED' and line_item.variant.product.tags.include?(DISCOUNT_TAG)) or DISCOUNT_TAG == 'NOT_NEEDED'
          # Tag prerequisite satisfied (if any) 
          if (SECOND_TIER_DISCOUNT_TYPE == 'PERCENTAGE')
            # Applying a percentage discount
            line_item.change_line_price((100 - SECOND_TIER_DISCOUNT_AMOUNT) * 0.01 * line_item.line_price, message: SECOND_TIER_DISCOUNT_MESSAGE)
          else
            # Applying a fixed discount
            if (SECOND_TIER_DISCOUNT_QUANTITY > 0)
              # Applying an 'x for $y' type of discount
              line_price_amount = line_item.change_line_price(SECOND_TIER_DISCOUNT_AMOUNT * line_item.quantity * Money.new(cents:100), message: SECOND_TIER_DISCOUNT_MESSAGE)
            elsif (SECOND_TIER_FIXED_TYPE_LEVEL == 'UNIT')
              # Applying the discount to the units price
              line_price_amount = line_item.line_price - (Money.new(cents:100) * SECOND_TIER_DISCOUNT_AMOUNT * line_item.quantity)
              line_item.change_line_price(line_price_amount, message: SECOND_TIER_DISCOUNT_MESSAGE)        
            elsif (SECOND_TIER_FIXED_TYPE_LEVEL == 'LINE')
              # Applying the discount to the line quantity (rare)
              line_price_amount = line_item.line_price - (Money.new(cents:100) * SECOND_TIER_DISCOUNT_AMOUNT)
              line_item.change_line_price(line_price_amount, message: SECOND_TIER_DISCOUNT_MESSAGE)        
            elsif (FIRST_TIER_FIXED_TYPE_LEVEL == 'UNIT OVERRIDE')
              # The unit price will be overwritten by a fixed unit price if it's lower than the unit price for the item
              if Money.new(cents:100) * SECOND_TIER_DISCOUNT_AMOUNT * line_item.quantity < line_item.line_price
                line_price_amount = Money.new(cents:100) * SECOND_TIER_DISCOUNT_AMOUNT * line_item.quantity
                line_item.change_line_price(line_price_amount, message: SECOND_TIER_DISCOUNT_MESSAGE)
              end
            else
              # Applying the discount to the order level using a weighted discount at the line level
              line_price_amount = line_item.line_price - (line_item.line_price.cents / total_eligible_price.cents) * SECOND_TIER_DISCOUNT_AMOUNT * Money.new(cents:100)
              line_item.change_line_price(line_price_amount, message: SECOND_TIER_DISCOUNT_MESSAGE)
            end
          end
        end
      end
  elsif (FIRST_TIER_THRESHOLD_PRICE == 0 or (total_eligible_price >= Money.new(cents:100) * FIRST_TIER_THRESHOLD_PRICE)) and (FIRST_TIER_THRESHOLD_QTY == 0 or (FIRST_TIER_THRESHOLD_QTY > 0 and total_eligible_qty >= FIRST_TIER_THRESHOLD_QTY)) and (FIRST_TIER_DISCOUNT_QUANTITY == 0 or (FIRST_TIER_DISCOUNT_QUANTITY > 0 and total_eligible_qty >= FIRST_TIER_DISCOUNT_QUANTITY))
      
      # Make sure that the items are sorted by quantity in case we want to split in the future
      eligible_items = eligible_items.sort_by { | line_item | line_item.quantity }
      
      # Apply the first tier discount (if it qualifies)
      Input.cart.line_items.each do |line_item|
        if (DISCOUNT_TAG != 'NOT_NEEDED' and line_item.variant.product.tags.include?(DISCOUNT_TAG)) or DISCOUNT_TAG == 'NOT_NEEDED'
          # Tag prerequisite satisfied (if any) 
          if (FIRST_TIER_DISCOUNT_TYPE == 'PERCENTAGE')
            # Applying a fixed discount
            line_item.change_line_price((100 - FIRST_TIER_DISCOUNT_AMOUNT) * 0.01 * line_item.line_price, message: FIRST_TIER_DISCOUNT_MESSAGE)
          else
            if (FIRST_TIER_DISCOUNT_QUANTITY > 0)
              # Applying an 'x for $y' type of discount
              line_price_amount = line_item.change_line_price(FIRST_TIER_DISCOUNT_AMOUNT * line_item.quantity * Money.new(cents:100), message: FIRST_TIER_DISCOUNT_MESSAGE)
            elsif (FIRST_TIER_FIXED_TYPE_LEVEL == 'UNIT')
              # Applying the discount to the units price
              line_price_amount = line_item.line_price - (Money.new(cents:100) * FIRST_TIER_DISCOUNT_AMOUNT * line_item.quantity)
              line_item.change_line_price(line_price_amount, message: FIRST_TIER_DISCOUNT_MESSAGE)
            elsif (FIRST_TIER_FIXED_TYPE_LEVEL == 'UNIT OVERRIDE')
              # The unit price will be overwritten by a fixed unit price if it's lower than the unit price for the item
              if Money.new(cents:100) * FIRST_TIER_DISCOUNT_AMOUNT * line_item.quantity < line_item.line_price
                line_price_amount = Money.new(cents:100) * FIRST_TIER_DISCOUNT_AMOUNT * line_item.quantity
                line_item.change_line_price(line_price_amount, message: FIRST_TIER_DISCOUNT_MESSAGE)
              end
            elsif (FIRST_TIER_FIXED_TYPE_LEVEL == 'LINE')
              # Applying the discount to the line quantity (rare)
              line_price_amount = line_item.line_price - (Money.new(cents:100) * FIRST_TIER_DISCOUNT_AMOUNT)
              line_item.change_line_price(line_price_amount, message: FIRST_TIER_DISCOUNT_MESSAGE)
            else
              # Applying the discount to the order level using a weighted discount at the line level
              line_price_amount = line_item.line_price - (line_item.line_price.cents / total_eligible_price.cents) * FIRST_TIER_DISCOUNT_AMOUNT * Money.new(cents:100)
              line_item.change_line_price(line_price_amount, message: FIRST_TIER_DISCOUNT_MESSAGE)              
            end
          end
        end
      end
  end
end

#DO NOT REMOVE
Output.cart = Input.cart