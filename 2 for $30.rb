# #################################################################
# This applies a discount where if customer buys more than one 'limited time offer items', it will make the price of 
# all those items be $15, except if there is an odd number, where the remaining one will be the original price.
#
# Maintenance Log
# Date           Description
# 2020/08/21     Created - Ray D/Juan
# #################################################################

# #################################################################
# Script Constants
# #################################################################
DISCOUNT_PRICE = 15
CART_DISCOUNT_MESSAGE = "2 FOR $30 POLOS"
DISCOUNT_TAG = "limited-time-offers"

# #################################################################
# Script Logic
# #################################################################

# Get all the elgible items, which are those that meet the "limited time offer" criteria
eligible_items = Input.cart.line_items.select do |line_item|
  product = line_item.variant.product
  !product.gift_card? && product.tags.include?(DISCOUNT_TAG)

  #FOR TEST CASES - Could not use one of the limited-time-offers tagged items so used the following instead
  #!product.gift_card? && product.tags.include?('BIGTALL2499')
  #!product.gift_card? && product.tags.include?('limited-time-offers')
end

# Get the total quantity for the eligible items
total_items = 0
eligible_items.each do |line_item|
  total_items += line_item.quantity
end

# don't apply any discount unless there is at least 2 eligible items in the cart
if (total_items >= 2)
  
  # let's sort the eligible items by quantities and grab the one with the most quantities
  # so that we can mark the eligible line that we would like to split if necessary 
  # and not have to worry about keep tracking of splitting lines that have lower quantities
  eligible_items = eligible_items.sort_by { |line_item| line_item.quantity }
  eligible_line_item_to_split = eligible_items.last
  
  # explicitly convert the DISCOUNT_PRICE to cents by multiplying by 100 cents
  total_discount = DISCOUNT_PRICE * Money.new(cents: 100)

  # flag that we haven't done a split for any of the line items
  splitItem = false;

  # loop through all the cart items
  Input.cart.line_items.each do |line_item|

    # loop through all eligible cart items found   
    eligible_items.each do |eligible_line_item|
      
      # to apply a discount, we need to make sure that current cart item is an eligible item
      if (line_item.variant == eligible_line_item.variant)

        # if total eligible items found is odd 
        #    and there is more than 1 eligible item in the current cart line
        #    and it is the eligible item that would require a potential split in the cart
        # make sure we discount all but 1 item by splitting the line into 2 cart lines.
        # the first line will have the dicounted eligible items
        # the second line will have the remaining 1 item
        if (total_items % 2 != 0 && line_item.quantity > 1 && line_item.variant == eligible_line_item_to_split.variant)
          
          # split the one remaining from the line items
          split_line_item = line_item.split(take: 1)
          
          # change the price for the first n - 1 number of line items
          line_item.change_line_price(total_discount * line_item.quantity, message: CART_DISCOUNT_MESSAGE)
          
          # get the current position of the current line number
          position =  Input.cart.line_items.find_index(line_item)
          
          # Create new line right after then current line item, which will contain the 1 remaining item that we split
          Input.cart.line_items.insert(position + 1, split_line_item)           
          
          # flag that a line split was done in the cart
          splitItem = true;
          
        elsif (splitItem == false || line_item.variant != eligible_line_item_to_split.variant)
          
          # if we haven't split a line or if the current line item is not a line we want to split, give the discount automatically
          line_item.change_line_price(total_discount * line_item.quantity, message: CART_DISCOUNT_MESSAGE)
    
        end
        
        # Once we apply our logic above, let's move out to the next cart line
        break;

      end
    end
  end
end


######################################################################
#                      PROMOTION CODES                              #
######################################################################

if Input.cart.discount_code && Input.cart.discount_code.code == "CUBRADS"
  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
      next if product.gift_card?
      next unless product.tags.include?('CUBRADS')
      line_item.change_line_price(Money.new(cents:1499*line_item.quantity), message: "$14.99")
      end
end

if Input.cart.discount_code && Input.cart.discount_code.code == "CUDEAL"
  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
      next if product.gift_card?
      next unless product.tags.include?('Collection:Affiliates')
      line_item.change_line_price(Money.new(cents:1799*line_item.quantity), message: "$17.99")
      end
end

Output.cart = Input.cart