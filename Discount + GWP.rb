# Exclusions *************************

tags_excluded = ['Watches', 'Sale Watches', 'Final Sale', 'Fragrances', 'Promotion_Exclusions']

Input.cart.line_items.each do |line_item|
  product = line_item.variant.product
  next if product.gift_card?
  next unless tags_excluded.include?(line_item.variant.product.tags)
  case Input.cart.discount_code
  when CartDiscount::Percentage
    Input.cart.discount_code.reject({message: "Cannot be used with This Product"})
  when CartDiscount::FixedAmount
    Input.cart.discount_code.reject({message: "Cannot be used with this product"})
  end
end

# PEA Exclusions *************************

if Input.cart.discount_code && Input.cart.discount_code.code != nil
Input.cart.line_items.each do |line_item|
 product = line_item.variant.product
 next if product.gift_card?
 next unless product.tags.include?('PEA')
 case Input.cart.discount_code.code
 when 'EXTRA10'
 when 'EXTRA15'
 when 'PEIN40'
 else
  Input.cart.discount_code.reject({message: "Cannot be used with Perry Ellis America"}) if product.tags.include?('PEA')
 end
end
end

# GWP: PEA Free Bag with Fragrance ********************************

discounted_product = 4168486322254
products_needed = [4168401780814]
products_seen = []

Input.cart.line_items.each do |line_item|
  product = line_item.variant.product
  products_seen << product.id if products_needed.include?(product.id)
end

Input.cart.line_items.each do |line_item|
  product = line_item.variant.product
  next unless product.id == discounted_product
  line_item.change_line_price(line_item.line_price - Money.new(cents:7500), message: "Free America Duffle Bag") if products_seen.uniq.sort == products_needed.uniq.sort
end

# GWP: Pure Blue Free Bag with Fragrance ********************************

discounted_product2 = 4450399813710
products_needed2 = [4038334677070]
products_seen2 = []

Input.cart.line_items.each do |line_item|
  product = line_item.variant.product
  products_seen2 << product.id if products_needed2.include?(product.id)
end

Input.cart.line_items.each do |line_item|
  product = line_item.variant.product
  next unless product.id == discounted_product2
  line_item.change_line_price(line_item.line_price - Money.new(cents:7500), message: "Free Pure Blue Duffle Bag") if products_seen2.uniq.sort == products_needed2.uniq.sort
end

# 40% OFF DRESS SHIRTS & PANT *************************
if Input.cart.discount_code && Input.cart.discount_code.code == "PESIGNUP"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "PEWELCOME"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "VOLUNTEER"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "PEIN40"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "PEIN55"
else
# 40% OFF DRESS SHIRTS & PANT *************************
    @percent = Decimal.new(40) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_FDS2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 40% OFF")
    end
    @percent = Decimal.new(40) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_FDP2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 40% OFF")
    end
# 50% OFF CASUAL SHIRTS - SWEATERS - CASUAL PANTS & DENIM *************************
    @percent = Decimal.new(50) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_FCS2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 50% OFF")
    end
    @percent = Decimal.new(50) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_SFO2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 50% OFF")
    end
    @percent = Decimal.new(50) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_FOCP2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 50% OFF")
    end
    @percent = Decimal.new(50) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_FPD2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 50% OFF")
    end
# 60% OFF OUTERWEAR *************************
    @percent = Decimal.new(60) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless product.tags.include?('Promotion_UTSOO2019')
    line_discount = line_item.line_price * @percent
    line_item.change_line_price(line_item.line_price - line_discount, message: "EXTRA 60% OFF")
    end
end

Output.cart = Input.cart