########### 15% Off with CU15 ###########

if Input.cart.discount_code && Input.cart.discount_code.code == "CU15"
  @percent = Decimal.new(15) / 100.0
    Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
        next if product.gift_card?
        next unless product.tags.include?('limited-time-offers')
            line_discount = line_item.line_price * @percent
            line_item.change_line_price(line_item.line_price - line_discount, message: "15% Off")
        end
end

Output.cart = Input.cart