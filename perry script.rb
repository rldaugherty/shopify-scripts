# **
# Bundle specific products (JP - 11/20/2020)
# **
  bundle_upcs = ['HEFM0012','HEN95K08','HEFS0003','HEHS0250','HEHS0500']
  bundle_prod_ids = [4876213715022,4874917838926,4783614623822,4875262427214,4875262492750]
  bundle_thresholds = [2000,1000,200,48,24]
  bundle_discounted_prices = [12,75,150,271,375]

  Input.cart.line_items.each do |line_item|
    prod_id = line_item.variant.product.id

    bundle_prod_ids.each_with_index do |bundle_prod_id, index|  
      if line_item.quantity >= bundle_thresholds[index] and prod_id == bundle_prod_id 
        line_amount = line_item.quantity * bundle_discounted_prices[index]
        line_item.change_line_price(Money.new(cents: line_amount), message: "Bundle Discount")
      end 
    end
  end
  
# Exclusions *

tags_excluded = ['Watches', 'Sale Watches', 'Final Sale', 'Fragrances', 'Promotion_Exclusions']

  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next if product.gift_card?
    next unless tags_excluded.include?(line_item.variant.product.tags)
    case Input.cart.discount_code
    when CartDiscount::Percentage
      Input.cart.discount_code.reject({message: "Cannot be used with This Product"})
    when CartDiscount::FixedAmount
      Input.cart.discount_code.reject({message: "Cannot be used with this product"})
    end
  end

# PEA Exclusions *

  if Input.cart.discount_code && Input.cart.discount_code.code != nil
  Input.cart.line_items.each do |line_item|
   product = line_item.variant.product
   next if product.gift_card?
   next unless product.tags.include?('PEA')
   case Input.cart.discount_code.code
   when 'EXTRA10'
   when 'EXTRA15'
   when 'PEIN40'
   when 'RMNPE'
   else
    Input.cart.discount_code.reject({message: "Cannot be used with Perry Ellis America"}) if product.tags.include?('PEA')
   end
  end
  end

# GWP: PEA Free Bag with Fragrance *

  discounted_product = 4168486322254
  products_needed = [4168401780814]
  products_seen = []

  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    products_seen << product.id if products_needed.include?(product.id)
  end

  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next unless product.id == discounted_product
    line_item.change_line_price(line_item.line_price - Money.new(cents:7500), message: "Free America Duffle Bag") if products_seen.uniq.sort == products_needed.uniq.sort
  end

# GWP: Pure Blue Free Bag with Fragrance *

  discounted_product2 = 4450399813710
  products_needed2 = [4038334677070]
  products_seen2 = []

  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    products_seen2 << product.id if products_needed2.include?(product.id)
  end

  Input.cart.line_items.each do |line_item|
    product = line_item.variant.product
    next unless product.id == discounted_product2
    line_item.change_line_price(line_item.line_price - Money.new(cents:7500), message: "Free Pure Blue Duffle Bag") if products_seen2.uniq.sort == products_needed2.uniq.sort
  end
  
  
    # 3 for $18 Masks *

if Input.cart.discount_code && Input.cart.discount_code.code == "PESIGNUP"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "PEWELCOME"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "MISSYOU"
else


DISCOUNT_PRICE = 6
CART_DISCOUNT_MESSAGE = "3 FOR $18 Masks"
DISCOUNT_TAG = "promotionmaskbundle"


eligible_3_for_30_items = Input.cart.line_items.select do |line_item|
  product = line_item.variant.product
  !product.gift_card? && product.tags.include?(DISCOUNT_TAG)

end

total_items = 0
eligible_3_for_30_items.each do |line_item|
  total_items += line_item.quantity
end

if (total_items >= 3)
  
  eligible_3_for_30_items = eligible_3_for_30_items.sort_by { | line_item | line_item.quantity }
  eligible_line_item_to_split = eligible_3_for_30_items.last
  
  total_discount = DISCOUNT_PRICE * Money.new(cents: 100)

  splitItem = false;

  Input.cart.line_items.each do |line_item|

    eligible_3_for_30_items.each do |eligible_line_item|
      
      if (line_item.variant == eligible_line_item.variant)

        if (total_items % 3 != 0 && line_item.quantity > 3 && line_item.variant == eligible_line_item_to_split.variant)
          
          if (line_item.quantity % 3 == 1)
            split_line_item = line_item.split(take: 1)
          elsif (line_item.quantity % 3 == 2)
            split_line_item = line_item.split(take: 2)
          end
            
          line_item.change_line_price(total_discount * line_item.quantity, message: CART_DISCOUNT_MESSAGE)
          
          position =  Input.cart.line_items.find_index(line_item)
          
          Input.cart.line_items.insert(position + 1, split_line_item)           

          splitItem = true;
          
        elsif (splitItem == false || line_item.variant != eligible_line_item_to_split.variant)
          
          line_item.change_line_price(total_discount * line_item.quantity, message: CART_DISCOUNT_MESSAGE)
          
        end
        
        break;

      end
    end
  end
end

end


    # 2 for $20 or 3 for $25 Underwear *
class Campaign
  def initialize(condition, *qualifiers)
    @condition = (condition.to_s + '?').to_sym
    @qualifiers = PostCartAmountQualifier ? [] : [] rescue qualifiers.compact
    @line_item_selector = qualifiers.last unless @line_item_selector
    qualifiers.compact.each do |qualifier|
      is_multi_select = qualifier.instance_variable_get(:@conditions).is_a?(Array)
      if is_multi_select
        qualifier.instance_variable_get(:@conditions).each do |nested_q|
          @post_amount_qualifier = nested_q if nested_q.is_a?(PostCartAmountQualifier)
          @qualifiers << qualifier
        end
      else
        @post_amount_qualifier = qualifier if qualifier.is_a?(PostCartAmountQualifier)
        @qualifiers << qualifier
      end
    end if @qualifiers.empty?
  end

  def qualifies?(cart)
    return true if @qualifiers.empty?
    @unmodified_line_items = cart.line_items.map do |item|
      new_item = item.dup
      new_item.instance_variables.each do |var|
        val = item.instance_variable_get(var)
        new_item.instance_variable_set(var, val.dup) if val.respond_to?(:dup)
      end
      new_item
    end if @post_amount_qualifier
    @qualifiers.send(@condition) do |qualifier|
      is_selector = false
      if qualifier.is_a?(Selector) || qualifier.instance_variable_get(:@conditions).any? { |q| q.is_a?(Selector) }
        is_selector = true
      end rescue nil
      if is_selector
        raise "Missing line item match type" if @li_match_type.nil?
        cart.line_items.send(@li_match_type) { |item| qualifier.match?(item) }
      else
        qualifier.match?(cart, @line_item_selector)
      end
    end
  end

  def run_with_hooks(cart)
    before_run(cart) if respond_to?(:before_run)
    run(cart)
    after_run(cart)
  end

  def after_run(cart)
    @discount.apply_final_discount if @discount && @discount.respond_to?(:apply_final_discount)
    revert_changes(cart) unless @post_amount_qualifier.nil? || @post_amount_qualifier.match?(cart)
  end

  def revert_changes(cart)
    cart.instance_variable_set(:@line_items, @unmodified_line_items)
  end
end

class TieredDiscount < Campaign
  def initialize(condition, customer_qualifier, cart_qualifier, line_item_selector, discount_type, tier_type, discount_tiers)
    super(condition, customer_qualifier, cart_qualifier)
    @line_item_selector = line_item_selector
    @discount_type = discount_type
    @tier_type = tier_type
    @discount_tiers = discount_tiers.sort_by {|tier| tier[:discount].to_f }
  end

  def init_discount(amount, message)
    case @discount_type
      when :fixed
        return FixedTotalDiscount.new(amount, message, :split)
      when :percent
        return PercentageDiscount.new(amount, message)
      when :per_item
        return FixedItemDiscount.new(amount, message)
    end
  end

  def run(cart)
    return unless qualifies?(cart)

    applicable_items = cart.line_items.select { |item| @line_item_selector.nil? || @line_item_selector.match?(item) }
    case @tier_type
      when :customer_tag
        return if cart.customer.nil?
        customer_tags = cart.customer.tags.map(&:downcase)
        qualified_tiers = @discount_tiers.select { |tier| customer_tags.include?(tier[:tier].downcase) }
      when :cart_subtotal
        cart_total = cart.subtotal_price
        qualified_tiers = @discount_tiers.select { |tier| cart_total >= Money.new(cents: tier[:tier].to_i * 100) }
      when :discountable_total
        discountable_total = applicable_items.reduce(Money.zero) { |total, item| total + item.line_price }
        qualified_tiers = @discount_tiers.select { |tier| discountable_total >= Money.new(cents: tier[:tier].to_i * 100) }
      when :discountable_total_items
        discountable_quantity = applicable_items.reduce(0) { |total, item| total + item.quantity }
        qualified_tiers = @discount_tiers.select { |tier| discountable_quantity >= tier[:tier].to_i }
      when :cart_items
        cart_quantity = cart.line_items.reduce(0) { |total, item| total + item.quantity }
        qualified_tiers = @discount_tiers.select { |tier| cart_quantity >= tier[:tier].to_i }
    end

    if @tier_type == :line_quantity
      applicable_items.each do |item|
        qualified_tiers = @discount_tiers.select { |tier| item.quantity >= tier[:tier].to_i }
        next if qualified_tiers.empty?

        discount_amount = qualified_tiers.last[:discount].to_f
        discount_message = qualified_tiers.last[:message]
        discount = init_discount(discount_amount, discount_message)
        discount.apply(item)
        discount.apply_final_discount if discount.respond_to?(:apply_final_discount)
      end
    else
      return if qualified_tiers.empty?
      discount_amount = qualified_tiers.last[:discount].to_f
      discount_message = qualified_tiers.last[:message]

      @discount = init_discount(discount_amount, discount_message)
      applicable_items.each { |item| @discount.apply(item) }
    end
  end
end

class PercentageDiscount
  def initialize(percent, message)
    @discount = (100 - percent) / 100.0
    @message = message
  end

  def apply(line_item)
    line_item.change_line_price(line_item.line_price * @discount, message: @message)
  end
end

class FixedTotalDiscount
  def initialize(amount, message, behaviour = :to_zero)
    @amount = Money.new(cents: amount * 100)
    @message = message
    @discount_applied = Money.zero
    @all_items = []
    @is_split = behaviour == :split
  end

  def apply(line_item)
    if @is_split
      @all_items << line_item
    else
      return unless @discount_applied < @amount
      discount_to_apply = [(@amount - @discount_applied), line_item.line_price].min
      line_item.change_line_price(line_item.line_price - discount_to_apply, {message: @message})
      @discount_applied += discount_to_apply
    end
  end

  def apply_final_discount
    return if @all_items.length == 0
    total_items = @all_items.length
    total_quantity = 0
    total_cost = Money.zero
    @all_items.each do |item|
      total_quantity += item.quantity
      total_cost += item.line_price
    end
    @all_items.each_with_index do |item, index|
      discount_percent = item.line_price.cents / total_cost.cents
      if total_items == index + 1
        discount_to_apply = Money.new(cents: @amount.cents - @discount_applied.cents.floor)
      else
        discount_to_apply = Money.new(cents: @amount.cents * discount_percent)
      end
      item.change_line_price(item.line_price - discount_to_apply, {message: @message})
      @discount_applied += discount_to_apply
    end
  end
end

class FixedItemDiscount
  def initialize(amount, message)
    @amount = Money.new(cents: amount * 100)
    @message = message
  end

  def apply(line_item)
    per_item_price = line_item.variant.price
    per_item_discount = [(@amount - per_item_price), @amount].max
    discount_to_apply = [(per_item_discount * line_item.quantity), line_item.line_price].min
    line_item.change_line_price(line_item.line_price - discount_to_apply, {message: @message})
  end
end

class Selector
  def partial_match(match_type, item_info, possible_matches)
    match_type = (match_type.to_s + '?').to_sym
    if item_info.kind_of?(Array)
      possible_matches.any? do |possibility|
        item_info.any? do |search|
          search.send(match_type, possibility)
        end
      end
    else
      possible_matches.any? do |possibility|
        item_info.send(match_type, possibility)
      end
    end
  end
end

class ProductTagSelector < Selector
  def initialize(match_type, match_condition, tags)
    @match_condition = match_condition
    @invert = match_type == :does_not
    @tags = tags.map(&:downcase)
  end

  def match?(line_item)
    product_tags = line_item.variant.product.tags.to_a.map(&:downcase)
    case @match_condition
      when :match
        return @invert ^ ((@tags & product_tags).length > 0)
      else
        return @invert ^ partial_match(@match_condition, product_tags, @tags)
    end
  end
end

CAMPAIGNS = [
  TieredDiscount.new(
    :all,
    nil,
    nil,
    ProductTagSelector.new(
      :does,
      :match,
      ["twoforunderwear"]
    ),
    :per_item,
    :discountable_total_items,
    [{:tier => "2", :discount => "3", :message => "2 FOR $20"},	{:tier => "3", :discount => "4.66", :message => "3 FOR $25"}]
  )
].freeze

CAMPAIGNS.each do |campaign|
  campaign.run_with_hooks(Input.cart)
end

    # 3 for $10 or 6 for $15 Socks *
class Campaign
  def initialize(condition, *qualifiers)
    @condition = (condition.to_s + '?').to_sym
    @qualifiers = PostCartAmountQualifier ? [] : [] rescue qualifiers.compact
    @line_item_selector = qualifiers.last unless @line_item_selector
    qualifiers.compact.each do |qualifier|
      is_multi_select = qualifier.instance_variable_get(:@conditions).is_a?(Array)
      if is_multi_select
        qualifier.instance_variable_get(:@conditions).each do |nested_q|
          @post_amount_qualifier = nested_q if nested_q.is_a?(PostCartAmountQualifier)
          @qualifiers << qualifier
        end
      else
        @post_amount_qualifier = qualifier if qualifier.is_a?(PostCartAmountQualifier)
        @qualifiers << qualifier
      end
    end if @qualifiers.empty?
  end

  def qualifies?(cart)
    return true if @qualifiers.empty?
    @unmodified_line_items = cart.line_items.map do |item|
      new_item = item.dup
      new_item.instance_variables.each do |var|
        val = item.instance_variable_get(var)
        new_item.instance_variable_set(var, val.dup) if val.respond_to?(:dup)
      end
      new_item
    end if @post_amount_qualifier
    @qualifiers.send(@condition) do |qualifier|
      is_selector = false
      if qualifier.is_a?(Selector) || qualifier.instance_variable_get(:@conditions).any? { |q| q.is_a?(Selector) }
        is_selector = true
      end rescue nil
      if is_selector
        raise "Missing line item match type" if @li_match_type.nil?
        cart.line_items.send(@li_match_type) { |item| qualifier.match?(item) }
      else
        qualifier.match?(cart, @line_item_selector)
      end
    end
  end

  def run_with_hooks(cart)
    before_run(cart) if respond_to?(:before_run)
    run(cart)
    after_run(cart)
  end

  def after_run(cart)
    @discount.apply_final_discount if @discount && @discount.respond_to?(:apply_final_discount)
    revert_changes(cart) unless @post_amount_qualifier.nil? || @post_amount_qualifier.match?(cart)
  end

  def revert_changes(cart)
    cart.instance_variable_set(:@line_items, @unmodified_line_items)
  end
end

class TieredDiscount < Campaign
  def initialize(condition, customer_qualifier, cart_qualifier, line_item_selector, discount_type, tier_type, discount_tiers)
    super(condition, customer_qualifier, cart_qualifier)
    @line_item_selector = line_item_selector
    @discount_type = discount_type
    @tier_type = tier_type
    @discount_tiers = discount_tiers.sort_by {|tier| tier[:discount].to_f }
  end

  def init_discount(amount, message)
    case @discount_type
      when :fixed
        return FixedTotalDiscount.new(amount, message, :split)
      when :percent
        return PercentageDiscount.new(amount, message)
      when :per_item
        return FixedItemDiscount.new(amount, message)
    end
  end

  def run(cart)
    return unless qualifies?(cart)

    applicable_items = cart.line_items.select { |item| @line_item_selector.nil? || @line_item_selector.match?(item) }
    case @tier_type
      when :customer_tag
        return if cart.customer.nil?
        customer_tags = cart.customer.tags.map(&:downcase)
        qualified_tiers = @discount_tiers.select { |tier| customer_tags.include?(tier[:tier].downcase) }
      when :cart_subtotal
        cart_total = cart.subtotal_price
        qualified_tiers = @discount_tiers.select { |tier| cart_total >= Money.new(cents: tier[:tier].to_i * 100) }
      when :discountable_total
        discountable_total = applicable_items.reduce(Money.zero) { |total, item| total + item.line_price }
        qualified_tiers = @discount_tiers.select { |tier| discountable_total >= Money.new(cents: tier[:tier].to_i * 100) }
      when :discountable_total_items
        discountable_quantity = applicable_items.reduce(0) { |total, item| total + item.quantity }
        qualified_tiers = @discount_tiers.select { |tier| discountable_quantity >= tier[:tier].to_i }
      when :cart_items
        cart_quantity = cart.line_items.reduce(0) { |total, item| total + item.quantity }
        qualified_tiers = @discount_tiers.select { |tier| cart_quantity >= tier[:tier].to_i }
    end

    if @tier_type == :line_quantity
      applicable_items.each do |item|
        qualified_tiers = @discount_tiers.select { |tier| item.quantity >= tier[:tier].to_i }
        next if qualified_tiers.empty?

        discount_amount = qualified_tiers.last[:discount].to_f
        discount_message = qualified_tiers.last[:message]
        discount = init_discount(discount_amount, discount_message)
        discount.apply(item)
        discount.apply_final_discount if discount.respond_to?(:apply_final_discount)
      end
    else
      return if qualified_tiers.empty?
      discount_amount = qualified_tiers.last[:discount].to_f
      discount_message = qualified_tiers.last[:message]

      @discount = init_discount(discount_amount, discount_message)
      applicable_items.each { |item| @discount.apply(item) }
    end
  end
end

class PercentageDiscount
  def initialize(percent, message)
    @discount = (100 - percent) / 100.0
    @message = message
  end

  def apply(line_item)
    line_item.change_line_price(line_item.line_price * @discount, message: @message)
  end
end

class FixedTotalDiscount
  def initialize(amount, message, behaviour = :to_zero)
    @amount = Money.new(cents: amount * 100)
    @message = message
    @discount_applied = Money.zero
    @all_items = []
    @is_split = behaviour == :split
  end

  def apply(line_item)
    if @is_split
      @all_items << line_item
    else
      return unless @discount_applied < @amount
      discount_to_apply = [(@amount - @discount_applied), line_item.line_price].min
      line_item.change_line_price(line_item.line_price - discount_to_apply, {message: @message})
      @discount_applied += discount_to_apply
    end
  end

  def apply_final_discount
    return if @all_items.length == 0
    total_items = @all_items.length
    total_quantity = 0
    total_cost = Money.zero
    @all_items.each do |item|
      total_quantity += item.quantity
      total_cost += item.line_price
    end
    @all_items.each_with_index do |item, index|
      discount_percent = item.line_price.cents / total_cost.cents
      if total_items == index + 1
        discount_to_apply = Money.new(cents: @amount.cents - @discount_applied.cents.floor)
      else
        discount_to_apply = Money.new(cents: @amount.cents * discount_percent)
      end
      item.change_line_price(item.line_price - discount_to_apply, {message: @message})
      @discount_applied += discount_to_apply
    end
  end
end

class FixedItemDiscount
  def initialize(amount, message)
    @amount = Money.new(cents: amount * 100)
    @message = message
  end

  def apply(line_item)
    per_item_price = line_item.variant.price
    per_item_discount = [(@amount - per_item_price), @amount].max
    discount_to_apply = [(per_item_discount * line_item.quantity), line_item.line_price].min
    line_item.change_line_price(line_item.line_price - discount_to_apply, {message: @message})
  end
end

class Selector
  def partial_match(match_type, item_info, possible_matches)
    match_type = (match_type.to_s + '?').to_sym
    if item_info.kind_of?(Array)
      possible_matches.any? do |possibility|
        item_info.any? do |search|
          search.send(match_type, possibility)
        end
      end
    else
      possible_matches.any? do |possibility|
        item_info.send(match_type, possibility)
      end
    end
  end
end

class ProductTagSelector < Selector
  def initialize(match_type, match_condition, tags)
    @match_condition = match_condition
    @invert = match_type == :does_not
    @tags = tags.map(&:downcase)
  end

  def match?(line_item)
    product_tags = line_item.variant.product.tags.to_a.map(&:downcase)
    case @match_condition
      when :match
        return @invert ^ ((@tags & product_tags).length > 0)
      else
        return @invert ^ partial_match(@match_condition, product_tags, @tags)
    end
  end
end

CAMPAIGNS = [
  TieredDiscount.new(
    :all,
    nil,
    nil,
    ProductTagSelector.new(
      :does,
      :match,
      ["sockbundle"]
    ),
    :per_item,
    :discountable_total_items,
    [{:tier => "3", :discount => "1.66", :message => "3 FOR $10"},	{:tier => "6", :discount => "2.49", :message => "6 FOR $15"}]
  )
].freeze

CAMPAIGNS.each do |campaign|
  campaign.run_with_hooks(Input.cart)
end

  
    # 2 for $50 Dress Pants *

if Input.cart.discount_code && Input.cart.discount_code.code == "PESIGNUP"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "PEWELCOME"
elsif Input.cart.discount_code && Input.cart.discount_code.code == "MISSYOU"
else
Input.cart.line_items.each_with_index do |line_item, index|

    items_quantities = 0

    # Loop over all the items to find eligble ones and total
    eligible_items = Input.cart.line_items.select do |line_item|
        product = line_item.variant.product
        !product.gift_card? && product.tags.include?('promo2for55')
    end

    total_items = 0

    eligible_items.each do |line_item|
      total_items += line_item.quantity
    end

    if total_items > 1
      Input.cart.line_items.each do |line_item|
        product = line_item.variant.product
        next unless product.tags.include?('promo2for55')
        line_item.change_line_price(Money.new(cents:2500*line_item.quantity), message: "2 for $50")
      end
    end

end
end

Output.cart = Input.cart