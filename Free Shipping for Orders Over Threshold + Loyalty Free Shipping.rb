
# FREE SHIPPING WITH $100 PURCHASE *******************************
ELIGIBLE_2DAY_SERVICES = ['Ground','Standard']

min_discount_order_amount = Money.new(cents:100) * 100
total = Input.cart.subtotal_price

new_total = total
discount_code = Input.cart&.discount_code

if discount_code
  if discount_code&.is_a?(CartDiscount::FixedAmount)
    discount_amount = discount_code&.amount
    new_total = total - discount_amount
  elsif discount_code&.is_a?(CartDiscount::Percentage)
    discount_percentage = 1.to_d - (discount_code&.percentage / 100)
    new_total = total * discount_percentage
  end
end

discount = if new_total > min_discount_order_amount
              1
            else
              0
            end
message = "Free Shipping On Orders $100+"

Input.shipping_rates.each do |shipping_rate|
  next unless shipping_rate.source == "shopify"
  if ELIGIBLE_2DAY_SERVICES.include?(shipping_rate.name)
    shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
  end
end

# FREE LOYALTY SHIPPING ********************************

  ELIGIBLE_SERVICES = ['Ground','Standard']
  ELIGIBLE_SERVICES2 = ['Ground','Standard','2nd Day']

  VIP_CUSTOMER_TAG0 = 'accepts_loyalty'
  VIP_CUSTOMER_TAG1 = 'silver'
  VIP_CUSTOMER_TAG2 = 'gold'

  # OVER $50+
  min_discount_order_amount = Money.new(cents:100) * 50
  total = Input.cart.subtotal_price

  new_total = total
  discount_code = Input.cart&.discount_code

  if discount_code
    if discount_code&.is_a?(CartDiscount::FixedAmount)
      discount_amount = discount_code&.amount
      new_total = total - discount_amount
    elsif discount_code&.is_a?(CartDiscount::Percentage)
      discount_percentage = 1.to_d - (discount_code&.percentage / 100)
      new_total = total * discount_percentage
    end
  end

  discount = if new_total > min_discount_order_amount
                1
              else
                0
              end
  message = "Free Shipping with $50+ purchase for Good Life Rewards Members."

  if new_total < min_discount_order_amount
  end

  if !Input.cart.customer.nil? and Input.cart.customer.tags.include?(VIP_CUSTOMER_TAG0)
    Input.shipping_rates.each do |shipping_rate|
      if ELIGIBLE_SERVICES.include?(shipping_rate.name)
        shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
      end
    end
  end

  if !Input.cart.customer.nil? and Input.cart.customer.tags.include?(VIP_CUSTOMER_TAG1)
    Input.shipping_rates.each do |shipping_rate|
      if ELIGIBLE_SERVICES.include?(shipping_rate.name)
        shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
      end
    end
  end

  if !Input.cart.customer.nil? and Input.cart.customer.tags.include?(VIP_CUSTOMER_TAG2)
    Input.shipping_rates.each do |shipping_rate|
      if ELIGIBLE_SERVICES2.include?(shipping_rate.name)
        shipping_rate.apply_discount(shipping_rate.price * discount, message: message)
      end
    end
  end

# Shopify is forcing flat fee per location based on locations instead of per 
# order. In order to force a flat per order, a discount needs to be applied 
# to force the shipping rate back down to the correct flat fee equivalents.

  # Available services
  SERVICE_NAMES = ['Ground','Standard','2nd Day','Overnight']
  
  # Flat fee values for service names above, respectively
  SERVICE_AMOUNTS = [8,8,12,20]
  
  Input.shipping_rates.each do |shipping_rate|
    next unless shipping_rate.source == "shopify"
    
    # Make sure we iterate through all the available services to match against what is in Shopify
    SERVICE_NAMES.each_with_index do |service_name, index|
      if service_name == shipping_rate.name
      
        # Check the current shipping rate against the fixed shipping rate
        # and if it is greater than the expected fixed shipping rate, 
        # apply their difference as a discount to get to the correct 
        # fixed amount for the entire order
        fixed_shipping_rate = Money.new(cents:100) * SERVICE_AMOUNTS[index] 
        if fixed_shipping_rate < shipping_rate.price
          shipping_rate.apply_discount(shipping_rate.price - fixed_shipping_rate, message: message)
        end
      end
    end
  end

Output.shipping_rates = Input.shipping_rates